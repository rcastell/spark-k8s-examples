#
# Copyright 2018 CERN/Switzerland
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Example:
# ./sparkctl create ./jobs/tpcds.yaml

apiVersion: "sparkoperator.k8s.io/v1alpha1"
kind: SparkApplication
metadata:
  name: tpcds
  namespace: default
spec:
  type: Scala
  mode: cluster
  image: gitlab-registry.cern.ch/db/spark-service/docker-registry/spark:v2.4.0-hadoop3.1-examples
  imagePullPolicy: IfNotPresent
  mainClass: ch.cern.tpcds.BenchmarkSparkSQL
  mainApplicationFile: local:///opt/spark/examples/jars/spark-service-examples_2.11-0.3.0.jar
  mode: cluster
  # By default, using cern provided spark-operator,
  # you are authenticated to use bucket of your cluster {{ cluster-name }} using s3a://
  arguments:
  # working directory where data table reside (must exists and have tables directly)
    - "s3a:///{{ custom-bucket }}/TPCDS-TEST"
  # location to store results
    - "s3a:///{{ custom-bucket }}/TPCDS-TEST-RESULT"
  # Path to kit in the docker image
    - "/opt/tpcds-kit/tools"
  # Scale factor (in GB)
    - "1"
  # Number of iterations
    - "1"
  # Optimize queries
    - "false"
  # Filter queries, will run all if empty - "q23a-v2.4,q23b-v2.4"
    - "q23a-v2.4"
  # Logging set to WARN
    - "true"
  deps:
    jars:
      - local:///opt/spark/examples/jars/scala-logging_2.11-3.9.0.jar
      - local:///opt/spark/examples/jars/spark-sql-perf_2.11-0.5.0-SNAPSHOT.jar
      - local:///opt/spark/examples/jars/spark-measure_2.11-0.11.jar
  hadoopConf:
    # By default, using cern provided spark-operator,
    # you are authenticated to use bucket of your cluster {{ cluster-name }}
    # This settings allow you to authenticate to custom bucket {{ custom-bucket }} in cs3.cern.ch endpoint
    "fs.s3a.endpoint": {{ endpoint }}
    "fs.s3a.bucket.{{ custom-bucket }}.access.key": {{ access }}
    "fs.s3a.bucket.{{ custom-bucket }}.secret.key": {{ secret }}
  sparkConf:
    # Cloud specific - need to run with speculation to avoid strugglers
    "spark.speculation": "true"
    "spark.speculation.multiplier": "3"
    "spark.speculation.quantile": "0.9"
    # TPCDs Specific
    "spark.sql.broadcastTimeout": "7200"
    "spark.sql.crossJoin.enabled": "true"
    # S3 Specific config (remove if s3 not used)
    # We need it to speed up uploads, and outputcommiter/parquet to have consistent writes due to speculation
    "spark.hadoop.fs.s3a.connection.timeout": "1200000"
    "spark.hadoop.fs.s3a.path.style.access": "true"
    "spark.hadoop.fs.s3a.connection.maximum": "200"
    "spark.hadoop.fs.s3a.fast.upload": "true"
    "spark.sql.parquet.mergeSchema": "false"
    "spark.sql.parquet.filterPushdown": "true"
    "spark.hadoop.fs.s3a.committer.name": "directory"
    "spark.hadoop.fs.s3a.committer.staging.conflict-mode": "append"
    "spark.hadoop.mapreduce.outputcommitter.factory.scheme.s3a": "org.apache.hadoop.fs.s3a.commit.S3ACommitterFactory"
  driver:
    cores: 1
    coreLimit: "1000m"
    memory: "1024m"
    labels:
      version: 2.4.0
    serviceAccount: spark
  executor:
    instances: 1
    cores: 1
    memory: "1024m"
    labels:
      version: 2.4.0
  restartPolicy: Never
